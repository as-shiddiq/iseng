-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2020 at 06:54 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `contoh_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `hoby`
--

CREATE TABLE `hoby` (
  `id_hoby` int(11) NOT NULL,
  `hoby` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hoby`
--

INSERT INTO `hoby` (`id_hoby`, `hoby`) VALUES
(1, 'Rajin'),
(2, 'Bolos'),
(3, 'Titip Absen'),
(4, 'Sering Bertanya'),
(9, 'Rebahan');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id_mahasiswa` int(11) NOT NULL,
  `nama_mahasiswa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id_mahasiswa`, `nama_mahasiswa`) VALUES
(61, 'sdsdf'),
(62, 'Khairudin');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa_hoby`
--

CREATE TABLE `mahasiswa_hoby` (
  `id_mahasiswa_hoby` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  `id_hoby` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa_hoby`
--

INSERT INTO `mahasiswa_hoby` (`id_mahasiswa_hoby`, `id_mahasiswa`, `id_hoby`) VALUES
(1, 61, 2),
(2, 61, 3),
(3, 62, 1),
(4, 62, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hoby`
--
ALTER TABLE `hoby`
  ADD PRIMARY KEY (`id_hoby`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id_mahasiswa`);

--
-- Indexes for table `mahasiswa_hoby`
--
ALTER TABLE `mahasiswa_hoby`
  ADD PRIMARY KEY (`id_mahasiswa_hoby`),
  ADD KEY `id_mahasiswa` (`id_mahasiswa`,`id_hoby`),
  ADD KEY `id_hoby` (`id_hoby`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hoby`
--
ALTER TABLE `hoby`
  MODIFY `id_hoby` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id_mahasiswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `mahasiswa_hoby`
--
ALTER TABLE `mahasiswa_hoby`
  MODIFY `id_mahasiswa_hoby` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mahasiswa_hoby`
--
ALTER TABLE `mahasiswa_hoby`
  ADD CONSTRAINT `mahasiswa_hoby_ibfk_1` FOREIGN KEY (`id_hoby`) REFERENCES `hoby` (`id_hoby`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mahasiswa_hoby_ibfk_2` FOREIGN KEY (`id_mahasiswa`) REFERENCES `mahasiswa` (`id_mahasiswa`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

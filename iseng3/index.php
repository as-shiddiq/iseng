<?php
	$setDb['db_host']='127.0.0.1';
	$setDb['db_name']='contoh_db';
	$setDb['db_user']='root';
	$setDb['db_password']='';
	include '../_loader.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Iseng #3</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<body class="p-4">
	<div style="position: fixed;bottom:0;left:0;background: #3339;padding: 10px">
		<h3 class="text-white">
			Iseng #3 : Form Multiple Choice #Part1
		</h3>
	</div>
	<div class="wrapper">
		<div class="d-flex row align-item-center justify-content-center align-items-stretch" style="height: 100vh">
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<h5>Form Multiple Choice</h5>
				<hr>
				<form method="post">
					<div class="form-group row">
						<label class="col-md-4">Nama Mahasiswa</label>
						<div class="col-md-8">
							<input name="nama_mahasiswa" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-4">Hoby</label>
						<div class="col-md-8">
							<?php 
								$get=$db->ObjectBuilder()->get('hoby');
								foreach ($get as $row) {
							 ?>
							<div class="form-check">
							  <input class="form-check-input" name="hoby<?=$row->id_hoby?>" type="checkbox" value="<?=$row->id_hoby?>" id="defaultCheck<?=$row->id_hoby?>">
							  <label class="form-check-label" for="defaultCheck<?=$row->id_hoby?>">
							    <?=$row->hoby?> <small class="font-weight-bold">[ name="hoby<?=$row->id_hoby?>" ]</small>
							  </label>
							</div>
							<?php } ?>
						</div>									
					</div>
					<div class="form-group row">
						<div class="col-md-4"></div>
						<div class="col-md-8">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</div>
				</form>
				<hr>
				<h5>Result</h5>
				<hr>
				<code>
					<?php
					if ($_SERVER['REQUEST_METHOD'] === 'POST') {
						#cara 1
						var_dump($_POST);
						$data=[
							'nama_mahasiswa'=>$_POST['nama_mahasiswa']
						];
						echo "<hr><h4>Simpan Ke Mahasiswa</h4><hr>";
						var_dump($data);
						//simpan data mahsiswa
						$db->insert('mahasiswa',$data);
						//setelah data mahasiswa disimpan, lanjut ambil data tersebut untuk id_mahasiswa
						$db->orderBy('id_mahasiswa','DESC');
						$get=$db->ObjectBuilder()->getOne('mahasiswa');

						$id_mahasiswa=$get->id_mahasiswa;

						echo "<hr><h4>Simpan Ke Hoby Mahasiswa</h4><hr>";
						$get=$db->ObjectBuilder()->get('hoby');
						foreach ($get as $row) {
							$data=null;
							// cek apakah ada id hoby yang dipost
							if(isset($_POST['hoby'.$row->id_hoby])){
								echo 'Data diambil dari checkbox dengan <strong>name="hoby'.$row->id_hoby.'"</strong><br>';

								$id_hoby=$_POST['hoby'.$row->id_hoby];
								$data=[
									'id_mahasiswa'=>$id_mahasiswa,
									'id_hoby'=>$id_hoby
								];
								var_dump($data);
								echo '<br>';
								//simpan data hoby mahsiswa
								$db->insert('mahasiswa_hoby',$data);
							}
						}
					}
					?>
				</code>

				
			</div>
			<div class="col-md-5">
				<h5>Table</h5>
				<hr>
				<table class="table table-bordered">
					<tr>
						<td>ID</td>
						<td>Nama Mahasiswa</td>
						<td>Hoby</td>
					</tr>
					<?php 
						$get=$db->ObjectBuilder()->get('mahasiswa',null,'*,(SELECT GROUP_CONCAT(hoby) FROM mahasiswa_hoby x LEFT JOIN hoby y ON x.id_hoby=y.id_hoby WHERE x.id_mahasiswa=mahasiswa.id_mahasiswa) as hoby');
						// echo $db->getLastQuery();
						foreach ($get as $row) {
					 ?>
					 <tr>
					 	<td><?=$row->id_mahasiswa?></td>
					 	<td><?=$row->nama_mahasiswa?></td>
					 	<td><?=$row->hoby?></td>
					 </tr>

					<?php }?>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>

	</div>
</body>
</html>